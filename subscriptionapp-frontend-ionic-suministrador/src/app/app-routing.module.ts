import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  //{ path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)},
  { path: 'scan/:id', loadChildren: () => import('./pages/scan/scan.module').then( m => m.ScanPageModule)},
  { path: 'perfil/:id', loadChildren: () => import('./pages/perfil/perfil.module').then( m => m.PerfilPageModule)},
  {path: 'perfil-mod/:id',loadChildren: () => import('./pages/perfil-mod/perfil-mod.module').then( m => m.PerfilModPageModule)},
  {path: 'ventas/:id',loadChildren: () => import('./pages/ventas/ventas.module').then( m => m.VentasPageModule)},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
