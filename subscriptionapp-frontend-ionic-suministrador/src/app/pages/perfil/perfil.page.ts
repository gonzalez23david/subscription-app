import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScannerService } from 'src/app/services/scanner.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  information = null;
  argumento : string = null;
  suministrador = null;
  nombre = "";
  

  constructor(private activatedRoute : ActivatedRoute, private scannerService : ScannerService, private router : Router) { }

  ngOnInit() {
    this.argumento = this.activatedRoute.snapshot.paramMap.get("id");
    this.cargarPerfil(this.argumento);
  }

  ionViewDidEnter(){
    this.cargarPerfil(this.argumento);
  }

  editarPerfil(){
    this.router.navigate(['perfil-mod', this.argumento]);
  }

  consultarVentas(){
    this.router.navigate(['ventas', this.argumento]);
  }
  

  cargarPerfil(id:string){
    this.scannerService.consultarPerfil(id).subscribe(result =>{
      this.information = result;
      
      document.getElementById("nombre").innerHTML =this.information.nombre;
      document.getElementById("apellido1").innerHTML =this.information.apellido1;
      document.getElementById("apellido2").innerHTML =this.information.apellido2;
      document.getElementById("dni").innerHTML =this.information.dni;
      document.getElementById("correo").innerHTML =this.information.correoElectronico;
      document.getElementById("fechaN").innerHTML =this.information.fechaNacimiento;
      document.getElementById("telefono").innerHTML =this.information.telefono;
      document.getElementById("cuentaBancaria").innerHTML =this.information.cuentaBancaria;
      document.getElementById("pais").innerHTML =this.information.pais;
      document.getElementById("provincia").innerHTML =this.information.provincia;
      document.getElementById("localidad").innerHTML =this.information.localidad;
      document.getElementById("cp").innerHTML =this.information.cp;
      document.getElementById("direccion").innerHTML =this.information.direccion;
    });
  }

}
