import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import {Router} from '@angular/router'
import { ScannerService } from 'src/app/services/scanner.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.page.html',
  styleUrls: ['./ventas.page.scss'],
})
export class VentasPage implements OnInit {

  argumento : string = null;
  information = null;
  nombre : string = "";

  constructor(private activatedRoute : ActivatedRoute, 
              private router : Router,
              private scannerService : ScannerService) { }

  ngOnInit() {
    this.argumento = this.activatedRoute.snapshot.paramMap.get("id");
    this.cargarEjemplaresVendidos(this.argumento);
  }

  cargarEjemplaresVendidos(id:string){
    this.scannerService.consultarVentas(id).subscribe(result =>{
      this.information = result;
      console.log(this.information);
      this.nombre = this.information[0].idSuministrador.nombre;
    });
  }

}
