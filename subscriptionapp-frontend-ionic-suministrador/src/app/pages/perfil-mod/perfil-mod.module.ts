import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerfilModPageRoutingModule } from './perfil-mod-routing.module';

import { PerfilModPage } from './perfil-mod.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerfilModPageRoutingModule
  ],
  declarations: [PerfilModPage]
})
export class PerfilModPageModule {}
