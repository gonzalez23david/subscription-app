import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PerfilModPage } from './perfil-mod.page';

describe('PerfilModPage', () => {
  let component: PerfilModPage;
  let fixture: ComponentFixture<PerfilModPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilModPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PerfilModPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
