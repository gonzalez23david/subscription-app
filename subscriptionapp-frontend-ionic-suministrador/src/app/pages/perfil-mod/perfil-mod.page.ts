import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { ScannerService } from 'src/app/services/scanner.service'

@Component({
  selector: 'app-perfil-mod',
  templateUrl: './perfil-mod.page.html',
  styleUrls: ['./perfil-mod.page.scss'],
})
export class PerfilModPage implements OnInit {

  argumento : string = null;
  information = null;
  colorLetra : string = "";

  nombre = "";
  nnombre = "";
  ap1 = "";
  nap1 = "";
  ap2   = "";
  nap2 = "";
  dni = "";
  ndni = "";
  email = "";
  nemail = "";
  fnac = "";
  nfnac = "";
  tfno = "";
  ntfno = "";

  cba = "";
  ncba = "";
  pais = "";
  npais = "";
  provincia = "";
  nprovincia = "";
  localidad = "";
  nlocalidad = "";
  cp = "";
  ncp = "";
  dir = "";
  ndir = "";

  constructor(private router : Router, private activatedRoute : ActivatedRoute, private scannerService : ScannerService) { }

  ngOnInit() {
    this.argumento = this.activatedRoute.snapshot.paramMap.get("id");
    this.cargarPerfil(this.argumento);
  }

  cargarPerfil(id){
    this.scannerService.consultarPerfil(id).subscribe(result =>{
      this.information = result;
      this.nombre = this.information.nombre;
      this.ap1 = this.information.apellido1;
      this.ap2 = this.information.apellido2;
      this.dni = this.information.dni;
      this.email = this.information.correoElectronico;
      this.fnac = this.information.fechaNacimiento;
      this.tfno = this.information.telefono;
      this.cba=this.information.cuentaBancaria;
      this.pais=this.information.pais;
      this.provincia=this.information.provincia;
      this.localidad=this.information.localidad;
      this.cp=this.information.cp;
      this.dir=this.information.direccion;
    });
  }

  realizarCambios(){
      //Meter confimación de que al menos un campo este rellenado.
      var perfilAux = this.information;
      var bool : boolean = false;
      if(this.nnombre != ""){
        perfilAux.nombre = this.nnombre;
        bool = true;
      }
      if(this.nap1 != ""){
        perfilAux.apellido1 = this.nap1;
        bool = true;
      }
      if(this.nap2 != ""){
        perfilAux.apellido2 = this.nap2;
        bool = true;
      }
      if(this.ndni != ""){
        perfilAux.dni = this.ndni;
        bool = true;
      }
      if(this.nemail != ""){
        perfilAux.correoElectronico = this.nemail;
        bool = true;
      }
      if(this.nfnac != ""){
        perfilAux.fechaNacimiento = this.nfnac;
        bool = true;
      }
      if(this.ntfno != ""){
        perfilAux.telefono = this.ntfno;
        bool = true;
      }
      if(this.ncba != ""){
        perfilAux.cuentaBancaria = this.ncba;
        bool = true;
      }
      if(this.npais != ""){
        perfilAux.pais = this.npais;
        bool = true;
      }
      if(this.nprovincia != ""){
        perfilAux.provincia = this.nprovincia;
        bool = true;
      }
      if(this.nlocalidad != ""){
        perfilAux.localidad = this.nlocalidad;
        bool = true;
      }
      if(this.ncp != ""){
        perfilAux.cp = this.ncp;
        bool = true;
      }
      if(this.ndir != ""){
        perfilAux.direccion = this.ndir;
        bool = true;
      }
      
      if(bool == true){
        this.scannerService.modificarPerfil(perfilAux);
        this.colorLetra = "success"
        document.getElementById("conf").innerHTML="Modificación realizada"
      }else{
        this.colorLetra = "danger"
        document.getElementById("conf").innerHTML="No hay ningun cambio"
      }
    
  }

}
