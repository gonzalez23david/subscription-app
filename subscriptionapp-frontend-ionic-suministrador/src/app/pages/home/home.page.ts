import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { ScannerService } from 'src/app/services/scanner.service';
import { Md5 } from 'ts-md5/dist/md5'

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  id = "";
  pwd = "";
  error = "";
  perfil = null;
  ocultar : boolean = false;

  constructor(private router : Router, private scannerService : ScannerService) { }

  ngOnInit() {
  }

  irEscaner(){
    this.comprobarUsuario(this.id);
  }

  comprobarUsuario(id:string){
    document.getElementById("err").innerHTML ="";
    this.ocultar=true;
    this.scannerService.consultarPerfil(id).subscribe(result =>{
      this.ocultar=false;
      console.log("Dentro metodo");
      this.perfil = result;
      var hashPwd = Md5.hashStr(this.pwd);
      if(this.perfil.password == hashPwd){
        this.router.navigate(['/scan', id]);
      }else{
        console.log("ENTRA");
        document.getElementById("err").innerHTML ="Contraseña Incorrecta";
      }
    }, error =>{
      this.ocultar=false;
      document.getElementById("err").innerHTML ="La IP no es correcta";
    }
    );
    console.log("Final del metodo");

  }

  presentAlertPrompt() {
    const alert = document.createElement('ion-alert');
    alert.header = 'Introduce la IP';
    alert.inputs = [
      {
        name: 'name2',
        id: 'name2-id',
        placeholder: this.scannerService.getIP()
      }
    ];
    alert.buttons = [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm Cancel')
        }
      }, {
        text: 'Ok',
        handler: (alertData) => {
          if(alertData.name2 != ""){
            console.log("Nueva IP: "+alertData.name2);
            this.scannerService.setIP(alertData.name2);
          }
        }
      }
    ];
  
    document.body.appendChild(alert);
    return alert.present();
  }

}
