import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ToastController } from '@ionic/angular';
import { ScannerService } from 'src/app/services/scanner.service'
import { Router, ActivatedRoute } from '@angular/router'
import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-scan',
  templateUrl: './scan.page.html',
  styleUrls: ['./scan.page.scss'],
})
export class ScanPage implements OnInit {

  //Variable que contiene los parametros de la geolocalización
  options : GeolocationOptions;

  scannedCode = null;
  ejemplar = null;
  suministrador = null;
  id : string;
  estado : string;
  locSolicitud : string;

  //Variable que muestra los botones en pantalla
  ocultar : boolean = false;
  //Variable que guarda el resultado de la comprobación de hora y lugar
  comprobacion : boolean = false;

  colorHora : string = "";
  colorEnt : string = "";

  latCom:number;
  lngCom:number;

  ret : boolean;

  //Comprueba que la escucha esta activa
  metActivo : boolean = false;
  color : string = "danger";

  //Boolean del metodo escucha
  finTrans : boolean = false;
  //Variable que comprueba si el QR ha sido escaneado correctamente
  inicioTrans : boolean = false;
  ejemDevuelto : boolean = false;

  argumento : string = null;

  spinner : boolean = false;

  geo:string = "";//Color del mensaje de la geolocalización

  constructor(private barcodeScanner : BarcodeScanner, 
              private toastCtrl : ToastController,
              private router : Router,
              private activatedRoute : ActivatedRoute,
              private scannerService : ScannerService,
              private geolocation : Geolocation) { }

  //Metodo que se ejecuta al entrar en la vista
  ngOnInit() {
    this.argumento = this.activatedRoute.snapshot.paramMap.get("id");
  }

  //Metodo que se ejecuta cuando se abandona la vista.
  ionViewWillLeave(){
    this.finTrans=true;
  }

  //Metodo que limpia todos los mensajes que se muestran en pantalla entre una transaccion y otra
  clearPantalla(){
    this.estado="";
    document.getElementById("codigoEscaneado").innerHTML="";
    document.getElementById("ejem").innerHTML="";
    document.getElementById("boolHora").innerHTML="";
    document.getElementById("entregado").innerHTML="";
    document.getElementById("fin").innerHTML="";
    document.getElementById("entro").innerHTML="";
    this.metActivo=false;
    this.finTrans=true;
  }

  //Metodo que hace llamadas al servidor para actulizar la información del ejemplar
  async refrescarEstado(idE){
    //Hay que poner finTrans siempre a false antes de empezar porque aunque se cambie de ejemplar
    //no se cambia de pagina por lo que finTrans la segunda vez estaria en True y no entraria
    this.finTrans=false;
    this.inicioTrans=false;
    console.log("Entra" + this.finTrans);
    this.metActivo = true;
    this.color="success";
    while(this.finTrans == false){
      console.log("Estado (refrescar) = "+this.estado);
      this.scannerService.consultarEjemplar(idE).subscribe(async result =>{
          this.ejemplar = result;
          this.estado = this.ejemplar.estado;

          //En cuanto el QR es leido se asigna el suministrador
          if(this.estado == "A Disposicion"){
            this.ocultar=true;
            if(this.inicioTrans == false){
              this.inicioTrans=true;
              this.cargarSuministrador(this.argumento);
              await this.delay(1000);
            }
            if(this.ejemDevuelto == true){
              this.ejemDevuelto=false;
              this.colorEnt="danger";
              document.getElementById("entregado").innerHTML="Ejemplar Devuelto.";
            }
          }else{
            this.ocultar=false;
          }

          //Si la transaccion ha sido cancelada o caducada
          if(this.estado == "Disponible" && this.inicioTrans == true){
            this.colorEnt="danger";
            document.getElementById("entregado").innerHTML="Ejemplar No Disponible.";
            document.getElementById("fin").innerHTML="Transaccion Insatisfactoria.";
            this.metActivo=false;
            this.color="danger";
            this.finTrans=true;
          }

          //Finalización satisfactoria de la transacción
          if(this.estado == "Confirmado"){
            this.metActivo=false;
            this.color="danger";
            this.finTrans=true;
            this.colorEnt="success";
            document.getElementById("entregado").innerHTML="Ejemplar Confirmado.";
            document.getElementById("fin").innerHTML="Transacción Satisfactoria.";
          }
      });
      await this.delay(1000);
    }
  }

  //Metodo para navegar a la pagina del perfil
  verPerfil(){
    this.router.navigate(['/perfil', this.argumento]);
  }

  //Metodo que asocioa el ejemplar al suministrador
  async cargarSuministrador(id:string){
    console.log("ENTRO: "+id);
    this.scannerService.consultarPerfil(id).subscribe(result =>{
      console.log("SUMINISTRADOR DENTRO: "+result);
      this.suministrador = result;
    });
    await this.delay(1000);
    console.log("SUMINISTRADOR: "+this.suministrador);
    var ejemplarAux = this.ejemplar;
    ejemplarAux.idSuministrador=this.suministrador;
    this.scannerService.cargarEjemplarQR(ejemplarAux);
  }
  
  //Metodo que activa la camara para escanear el QR
  scanCode(){
    this.ejemplar = null;
    this.id = "";
    this.estado = "";
    this.barcodeScanner.scan().then(
      barcodeData => {
        this.scannedCode = barcodeData.text;
        document.getElementById("codigoEscaneado").innerHTML=this.scannedCode;
        this.consultarEjemplar(this.scannedCode);
      }
    );
  }

   //Metodo que recibe uel texto asociado a un QR y busca el ejemplar
   consultarEjemplar(codigoQR){
    this.scannerService.consultarEjemplarPorQR(codigoQR).subscribe(async result =>{
      this.ejemplar = result;
      this.estado = this.ejemplar.estado;
      this.id = this.ejemplar.idEjemplar;
      this.locSolicitud=this.ejemplar.localizacionSolicitud;
      //Comprueba que el QR leido corresponde a un ejemplar Solicitado
      if(this.estado == "Solicitado"){
        document.getElementById("ejem").innerHTML=this.ejemplar.idArticulo.nombre;
        var ejemplarAux = this.ejemplar;

        //Guardamos en momento de la entrega
        var fecha_entrega = new Date();
        var hora_entrega = fecha_entrega.getHours() + ":" + fecha_entrega.getMinutes() + ":" + fecha_entrega.getSeconds();
        console.log("Fecha Entrega: "+hora_entrega.toString());
        ejemplarAux.fechaEntrega = hora_entrega.toString();

        //Guardamos el lugar de la entrega
        this.spinner=true;
        ejemplarAux.localizacionEntrega=null;
        this.options = {enableHighAccuracy : true};
        this.geolocation.getCurrentPosition().then((geoposition : Geoposition) => {
          var lat = geoposition.coords.latitude;
          var lng = geoposition.coords.longitude;
          ejemplarAux.localizacionEntrega = lat+", "+lng;
        });

        //Espera para que le de tiempo a guardar la localizacion

        while(ejemplarAux.localizacionEntrega==null){
          await this.delay(100);
        }

        this.comprobacion=null;        
       this.comprobacion=this.comprobarFechaLugar(fecha_entrega,this.id,ejemplarAux.localizacionEntrega);
        this.spinner=false;

        if(this.comprobacion == true){
          ejemplarAux.estado="A Disposicion"
          this.estado = "A Disposicion";
          this.scannerService.cargarEjemplarQR(ejemplarAux);
          this.refrescarEstado(this.id);

        }else{
          //Si falla se resetea el ejemplar
          var ejemplarAuxFalse = this.ejemplar;
          ejemplarAuxFalse.codigoQR = null;
          ejemplarAuxFalse.fechaSolicitud = null;
          ejemplarAuxFalse.fechaEntrega = null;
          ejemplarAuxFalse.localizacionSolicitud = null;
          ejemplarAuxFalse.localizacionEntrega = null;
          ejemplarAuxFalse.estado = "Disponible"
          this.colorEnt="danger";
          document.getElementById("entregado").innerHTML="Comprobación no correcta.";
          document.getElementById("fin").innerHTML="Transacción Insatisfactoria.";
          this.scannerService.cargarEjemplarQR(ejemplarAuxFalse);
        }
      }
    });
  }
  
  //Metodo asociado al boton ENTREGAR
  entregarEjemplar(){
    var ejemplarAux=this.ejemplar;
    ejemplarAux.estado = "Entregado";
    this.scannerService.cargarEjemplarQR(ejemplarAux);
    this.colorEnt="success"
    document.getElementById("entregado").innerHTML="Ejemplar entregado.";
    this.ejemDevuelto = true;
  }

  //Metodo asociado al boton RECHAZAR
  rechazarEjemplar(){
    var ejemplarAux=this.ejemplar;
        //Si rechaza se resetea el ejemplar
        var ejemplarAuxFalse = this.ejemplar;
        ejemplarAuxFalse.codigoQR = null;
        ejemplarAuxFalse.fechaSolicitud = null;
        ejemplarAuxFalse.fechaEntrega = null;
        ejemplarAuxFalse.localizacionSolicitud = null;
        ejemplarAuxFalse.localizacionEntrega = null;
        ejemplarAuxFalse.idSuministrador = null;
        ejemplarAuxFalse.estado = "Disponible";
        this.colorEnt="danger"
        document.getElementById("entregado").innerHTML="Ejemplar rechazado.";
        this.scannerService.cargarEjemplarQR(ejemplarAuxFalse);
  }

  //Metodo que comprueba que la hora y lugar sean correctos
  comprobarFechaLugar(fecha_entrega, id:string, localizacionEntrega){
     //Realizar la comprobacion de hora y lugar
     var booleanHora : boolean = false;
     var booleanLugar : boolean = true;

     var fechaSolicitud = this.ejemplar.fechaSolicitud;

     //Separar en horas, minutos y segundos
     var array = fechaSolicitud.split(":");
     var hS = parseInt(array[0]);
     var mS = parseInt(array[1]);
     var sS = parseInt(array[2]);

     var hE = fecha_entrega.getHours();
     var mE = fecha_entrega.getMinutes();
     var sE = fecha_entrega.getSeconds();

     if((hS == hE) && (mS == mE)){
      booleanHora = true;
    }else if((mS == mE-1) && (sS >= sE)){
      booleanHora = true;
    }else{

    }
     //this.comprobarLugar(id,localizacionEntrega);
     //---------------------------------------------------
     //Comprueba la geolocalizacion
      var locSolicitud = this.locSolicitud;
      var locEntrega = localizacionEntrega;

      var arrSolicitud = locSolicitud.split(",");
      var arrEntrega = locEntrega.split(",");

      var latSolicitud : number = parseFloat(arrSolicitud[0]);
      var lngSolicitud : number = parseFloat(arrSolicitud[1]);
      var latEntrega : number = parseFloat(arrEntrega[0]);
      var lngEntrega : number = parseFloat(arrEntrega[1]);

      console.log(latSolicitud);
      console.log(lngSolicitud);
      console.log(latEntrega);
      console.log(lngEntrega);

      var latResta = (latSolicitud-latEntrega)*1000;
      var lngResta = (lngSolicitud-lngEntrega)*1000;
      console.log(latResta);
      console.log(lngResta);

      document.getElementById("entro").innerHTML="";
      this.ret=null;
      if((latResta <= 1 && latResta >= -1) && (lngResta <= 1 && lngResta >= -1)){
        this.ret = true;
      }else{
        this.ret = false;
      }
      console.log("RET:"+this.ret);
     //---------------------------------------------------

     if(this.ret){
      this.geo="success"
      document.getElementById("entro").innerHTML="Geolocalización correcta.";
     }else{
       this.geo="danger"
      document.getElementById("entro").innerHTML="Geolocalización incorrecta.";
     }

     var r:boolean;
     if(booleanHora && this.ret){
      this.colorHora = "success"
      document.getElementById("boolHora").innerHTML="Comprobaciones correctas."
      r=true;
     }else{
      this.colorHora = "danger"
      document.getElementById("boolHora").innerHTML="Comprobaciones incorrectas."
      r=false;
     }
     return r;
  }

  private delay(ms : number){
    return new Promise(resolve => setTimeout(resolve,ms));
  }
}
