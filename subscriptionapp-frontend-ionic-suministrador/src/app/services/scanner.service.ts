import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class ScannerService {

  //Tengo que mirar como hacer para obtener las ips
  //urlEjemplares = "http://localhost:8080/subscriptionapp-backend-java/subapp/ejemplares/";
  ip = "192.168.1.132"; 
  urlSuministrador = "http://"+this.ip+":8080/subscriptionapp-backend-java/subapp/suministradores/";
  urlEjemplares = "http://"+this.ip+":8080/subscriptionapp-backend-java/subapp/ejemplares/";
  urlEjemplaresPorQR="http://"+this.ip+":8080/subscriptionapp-backend-java/subapp/ejemplares/qr/";
  ejem = "/ejem";
  

  httpHeaders = {
    headers : new HttpHeaders({'Accept' : 'application/json','Content-Type' : 'application/json'})
  };

  constructor(private http : HttpClient) { }

  setIP(ip:string){
    this.ip = ip;
    this.urlSuministrador = "http://"+this.ip+":8080/subscriptionapp-backend-java/subapp/suministradores/";
    this.urlEjemplares = "http://"+this.ip+":8080/subscriptionapp-backend-java/subapp/ejemplares/";
    this.urlEjemplaresPorQR="http://"+this.ip+":8080/subscriptionapp-backend-java/subapp/ejemplares/qr/";
  }

  getIP(){
    return this.ip;
  }

  consultarVentas(id:string){
    return this.http.get(this.urlSuministrador+id+this.ejem);
  }

  consultarPerfil(id:string){
    return this.http.get(this.urlSuministrador+id);
  }

  consultarEjemplar(id:string){
    return this.http.get(this.urlEjemplares+id);
  }

  consultarEjemplarPorQR(cod:string){
    return this.http.get(this.urlEjemplaresPorQR+cod);
  }

  modificarPerfil(perfil){
    console.log("Perfil a modificar: "+perfil);
    this.http.put(this.urlSuministrador+perfil.idSuministrador,perfil,this.httpHeaders).subscribe(data =>{
      console.log("PUT bien:");
        console.log(data['_body']);
      }, error =>{
        console.log("PUT mal:");
        console.log(error);
    });
  }

  cargarEjemplarQR(ejemplar){
    console.log("Ejemplar a modificar: ");
    console.log(ejemplar);
    this.http.put(this.urlEjemplares+ejemplar.idEjemplar,ejemplar,this.httpHeaders).subscribe(
      data => {
        console.log("PUT bien:");
        console.log(data['_body']);
      }, error =>{
        console.log("PUT mal:");
        console.log(error);
      }
    );
  }

}
