# Subscription App

Trabajo fin de grado en la UBU de David González López.

En la actualidad, las suscripciones a prensa o a publicaciones de
cualquier índole están sometidas a las condiciones impuestas por las
editoriales. Muchas veces es necesario contar con una tarjeta de algún
tipo y un dispositivo capaz de reconocer dichas tarjetas para poder
recoger el ejemplar de la suscripción. Esto genera dependencias tanto
al suscriptor como al suministrador.
El objetivo del proyecto es dar respuesta a este problema mediante
el desarrollo de un sistema cliente servidor que mediante el intercambio
de mensajes seguros permita garantizar las transacciones de forma
eficaz a través de terminales móviles.
El alcance de este proyecto, debido a las limitaciones tanto temporales
como materiales, queda limitado a la realización de una prueba
de concepto (POC), que permita el desarrollo especifico de cada uno
de los componentes en próximos proyectos.
El proyecto cuenta con una base de datos donde se alojará la
información, una API REST que ofrece estos datos y dos clientes
Suscriptor y Suministrador que obtendrán y transformarán los datos
para llevar a cabo las transacciones y garantizar la seguridad
realizando comprobaciones geográficas y temporales.
Esta prueba de concepto ha consistido en la implementación,
de forma resumida, de la idea de la comunicación para verificar el
concepto en sí y posteriormente su funcionamiento. El núcleo del
proyecto es la comunicación y se ha intentado explotar esta idea al
máximo dentro de las limitaciones mencionadas anteriormente.
Una vez mencionado todo esto presento SubscriptionApp.

