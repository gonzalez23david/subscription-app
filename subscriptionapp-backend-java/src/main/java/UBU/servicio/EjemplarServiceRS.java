/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.servicio;

import UBU.data.EjemplarDao;
import UBU.domain.Ejemplar;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author david
 */

@Stateless
@Path("/ejemplares")
public class EjemplarServiceRS {
    
    @Inject
    private EjemplarDao ejemplarDao;
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<Ejemplar> encontrarTodosEjemplares(){
        List<Ejemplar> ejemplares = ejemplarDao.encontrarTodosEjemplares();
        System.out.println("Ejemplares encontrados: " + ejemplares);
        return ejemplares;
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Ejemplar encontrarEjemplar(@PathParam("id") Integer id){
        Ejemplar ejemplar = ejemplarDao.encontrarEjemplar(new Ejemplar(id));
        System.out.println("Ejemplar encontrado: "+ejemplar);
        return ejemplar;
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("/qr/{cod}")
    public Ejemplar encontrarEjemplarPorQR(@PathParam("cod") String cod){
        Ejemplar ejemplar = ejemplarDao.encontrarEjemplarPorQR(new Ejemplar(cod));
        System.out.println("Ejemplar encontrado por QR: " + ejemplar);
        return ejemplar;
    }
    
    @PUT
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response modificarEjemplar(@PathParam("id") Integer id, Ejemplar ejemplarModificado){
        Ejemplar ejemplar = ejemplarDao.encontrarEjemplar(new Ejemplar(id));
        if(ejemplar != null){
            ejemplarDao.actualizarEjemplar(ejemplarModificado);
            System.out.println("Ejemplar modificado: "+ ejemplarModificado);
            return Response.ok().entity(ejemplarModificado).build();
        }else{
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}
