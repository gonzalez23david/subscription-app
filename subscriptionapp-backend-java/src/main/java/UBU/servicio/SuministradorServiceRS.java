/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.servicio;

import UBU.data.SuministradorDao;
import UBU.data.SuscriptorDao;
import UBU.domain.Ejemplar;
import UBU.domain.Publicacion;
import UBU.domain.Suministrador;
import java.util.Collection;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author david
 */

@Stateless
@Path("/suministradores")
public class SuministradorServiceRS {
    
    @Inject
    private SuministradorDao suministradorDao;
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<Suministrador>listarSuministrador(){
        List<Suministrador> suministradores = suministradorDao.encontrarTodosSuministradores();
        System.out.println("Suministradores encontrados: "+suministradores);
        return suministradores;
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Suministrador encontrarSuministrador(@PathParam("id") Integer id){
        Suministrador suministrador = suministradorDao.encontrarSuministrador(new Suministrador(id));
        System.out.println("Suministrador encontrado: "+suministrador);
        return suministrador;
    }
    
    @PUT
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response modificarSuministrador(@PathParam("id") Integer id, Suministrador suministradorModificado){
        Suministrador suministrador = suministradorDao.encontrarSuministrador(new Suministrador(id));
        if(suministrador != null){
            suministradorDao.actualizarSuministrador(suministradorModificado);
            System.out.println("Suministrador modificado: "+suministradorModificado);
            return Response.ok().entity(suministradorModificado).build();
        }else{
            return Response.status(Status.NOT_FOUND).build();
        }
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}/ejem")
    public List<Ejemplar> encontrarTodosEjemplares(@PathParam("id") Integer id){
        List<Ejemplar> ejemplares = suministradorDao.encontrarTodosEjemplares(new Suministrador(id));
        System.out.println("Ejemplares encontrados: "+ejemplares);
        return ejemplares;
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}/pbl")
    public List<List<Publicacion>> encontrarTodasPublicaciones(@PathParam("id") Integer id){
        List<List<Publicacion>> publicaciones = suministradorDao.encontrarTodasPublicaciones(new Suministrador(id));
        System.out.println("Publicaciones encontradas: "+publicaciones);
        return publicaciones;
    }
 
}
