/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.servicio;

import UBU.data.EditorialDao;
import UBU.domain.Editorial;
import UBU.domain.Ejemplar;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author david
 */

@Stateless
@Path("/editoriales")
public class EditorialServiceRS {
    
    @Inject
    private EditorialDao editorialDao;
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<Editorial> listarEditoriales(){
        List<Editorial> editoriales = editorialDao.encontrarTodasEditoriales();
        System.out.println("Editoriales encontradas: "+editoriales);
        return editoriales;
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Editorial encontrarEditorial(@PathParam ("id") Integer id){
        Editorial editorial = editorialDao.encontrarEditorial(new Editorial(id));
        System.out.println("Editorial encontrada: "+editorial);
        return editorial;
    }
    /*
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}/ejem")
    public List<List<Ejemplar>> encontrarTodosEjemplares(@PathParam("id"), Integer id){
        List<List<Ejemplar>> ejemplares = editorialDao.encontrarTodosEjemplares(new Editorial(id));
        System.out.println("Ejemplares encontrados: " + ejemplares);
        return ejemplares;
    }
    */
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}/ejem")
    public List<Ejemplar> consultarEjemplares(@PathParam("id") Integer id){
        List<Ejemplar> ejemplares = editorialDao.encontrarTodosEjemplares(new Editorial(id));
        System.out.println("Ejemplares encontrados: " + ejemplares);
        return ejemplares;
    }
    
}
