/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.servicio;

import UBU.data.SuscriptorDao;
import UBU.domain.Ejemplar;
import UBU.domain.Publicacion;
import UBU.domain.Suscripcion;
import UBU.domain.Suscriptor;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author david
 */

@Stateless
@Path("/suscriptores")
public class SuscriptorServiceRS {
    
    @Inject
    private SuscriptorDao suscriptorDao;
    
    @GET
    @Produces(value=MediaType.APPLICATION_JSON)
    public List<Suscriptor> listarSuscriptores(){
        List<Suscriptor> suscriptores = suscriptorDao.encontrarTodosSuscriptores();
        System.out.println("suscriptores encontrados: "+suscriptores);
        return suscriptores;
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")   
    public Suscriptor encontrarSuscriptor(@PathParam("id") Integer id){
        Suscriptor suscriptor = suscriptorDao.encontrarSuscriptor(new Suscriptor(id));
        System.out.println("Suscriptor encontrado: "+suscriptor);
        return suscriptor;
    }
    
    @GET 
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}/scrnes")
    public List<Suscripcion> encontrarTodasSuscripciones(@PathParam("id") Integer id){
        List<Suscripcion> suscripciones = suscriptorDao.encontrarTodasSuscripciones(new Suscriptor(id));
        System.out.println("Suscripciones encontradas: "+suscripciones);
        return suscripciones;
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}/ejem")
    public List<Ejemplar> encontrarTodosEjemplares(@PathParam("id") Integer id){
        List<Ejemplar> ejemplares = suscriptorDao.encontrarTodosEjemplares(new Suscriptor(id));
        System.out.println("Ejemplares encontrados: " + ejemplares);
        return ejemplares;
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{idS}/ejem/{idP}")
    public List<Ejemplar> encontrarTodosEjemplaresPorPublicacion(@PathParam("idS") Integer idS, @PathParam("idP") Integer idP){
        List<Ejemplar> ejemplares = suscriptorDao.encontrarTodosEjemplaresPorPublicacion(new Suscriptor(idS), new Publicacion(idP));
        System.out.println("Ejemplares de la publicacion: "+idP+" encontrados: "+ejemplares);
        return ejemplares;
    }
    
    @PUT
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response modificarSuscriptor(@PathParam("id") Integer id, Suscriptor suscriptorModificado){
        Suscriptor suscriptor = suscriptorDao.encontrarSuscriptor(new Suscriptor(id));
        if(suscriptor != null){
            suscriptorDao.actualizarSuscriptor(suscriptorModificado);
            System.out.println("Suscriptor modificado: "+ suscriptorModificado);
            return Response.ok().entity(suscriptorModificado).build();
        }else{
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}
