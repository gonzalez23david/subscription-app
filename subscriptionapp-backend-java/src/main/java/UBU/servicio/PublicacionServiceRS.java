/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.servicio;

import UBU.data.PublicacionDao;
import UBU.domain.Editorial;
import UBU.domain.Publicacion;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author david
 */

@Stateless
@Path("/publicaciones")
public class PublicacionServiceRS {
    
    @Inject
    private PublicacionDao publicacionDao;
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Publicacion encontrarPublicacion(@PathParam("id") Integer id){
        Publicacion publicacion = publicacionDao.encontrarPublicacion(new Publicacion(id));
        System.out.println("Publicacion encontrada: "+publicacion);
        return publicacion;
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}/ed")
    public Editorial encontrarEditorialPublicacion(@PathParam("id") Integer id){
        Editorial editorial = publicacionDao.encontrarEditorialPublicacion(new Publicacion(id));
        return editorial;
    }
    
}
