/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.data;

import UBU.domain.Ejemplar;
import UBU.domain.Publicacion;
import UBU.domain.Suministrador;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author david
 */
public interface SuministradorDao {
    
    public List<Suministrador> encontrarTodosSuministradores();
    
    public Suministrador encontrarSuministrador(Suministrador suministrador);
    
    public void actualizarSuministrador(Suministrador suministrador);
    
    public List<Ejemplar> encontrarTodosEjemplares(Suministrador suministrador);
    
    public List<List<Publicacion>> encontrarTodasPublicaciones(Suministrador suministrador);
    
}
