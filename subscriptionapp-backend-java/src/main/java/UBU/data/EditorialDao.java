/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.data;

import UBU.domain.Editorial;
import UBU.domain.Ejemplar;
import java.util.List;

/**
 *
 * @author david
 */
public interface EditorialDao {
    
    public List<Editorial> encontrarTodasEditoriales();
    
    public Editorial encontrarEditorial(Editorial editorial);
    
    public List<Ejemplar> encontrarTodosEjemplares(Editorial editorial);
    
}
