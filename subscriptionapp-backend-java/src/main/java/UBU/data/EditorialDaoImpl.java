/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.data;

import UBU.domain.Editorial;
import UBU.domain.Ejemplar;
import UBU.domain.Suministrador;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author david
 */

@Stateless
public class EditorialDaoImpl implements EditorialDao{
    
    @PersistenceContext(unitName = "SubscriptionAppPU")
    EntityManager em;

    @Override
    public List<Editorial> encontrarTodasEditoriales() {
        return em.createNamedQuery("Editorial.findAll").getResultList();
    }

    @Override
    public Editorial encontrarEditorial(Editorial editorial) {
        return em.find(Editorial.class, editorial.getIdEditorial());
    }
    
    @Override
    public List<Ejemplar> encontrarTodosEjemplares(Editorial editorial) {
        /*
        List<Suministrador> suministradores = em.createNamedQuery("Editorial.encontrarTodosSuministradores").setParameter("idEditorial", editorial).getResultList();
        List<List<Ejemplar>> ejemplares = new ArrayList<>();
        for(Suministrador s : suministradores){
            ejemplares.add(em.createNamedQuery("Editorial.encontrarEjemplarPorSuministrador").setParameter("idSuministrador", s).getResultList());
        }
        return ejemplares;
        */
        Editorial edi;
        List<Ejemplar> ejemplares =  em.createNamedQuery("Ejemplar.findAll").getResultList();
        ArrayList<Ejemplar> ejemplaresAux = new ArrayList<Ejemplar>();
        for(Ejemplar e : ejemplares){
            edi=null;
            edi=e.getIdArticulo().getIdPublicacion().getIdEditorial();
            System.out.println("EDI: " + edi);
            if(edi.equals(editorial)){
                ejemplaresAux.add(e);
            }
        }
        return ejemplaresAux;
    }
    
}
