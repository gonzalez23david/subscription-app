/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.data;

import UBU.domain.Ejemplar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author david
 */
public class EjemplarDaoImpl implements EjemplarDao {
    
    @PersistenceContext(unitName = "SubscriptionAppPU")
    EntityManager em;
    
    @Override
    public List<Ejemplar> encontrarTodosEjemplares() {
        return em.createNamedQuery("Ejemplar.findAll").getResultList();
    }

    @Override
    public Ejemplar encontrarEjemplar(Ejemplar ejemplar) {
        return em.find(Ejemplar.class, ejemplar.getIdEjemplar());
    }
    
    @Override
    public Ejemplar encontrarEjemplarPorQR(Ejemplar ejemplar) {
        List<Ejemplar> ejemplares = encontrarTodosEjemplares();
        Ejemplar ejemplarRet = null;
        for(Ejemplar e : ejemplares){
            if(e.getCodigoQR()!="" || e.getCodigoQR()!=null){
                if(e.getCodigoQR() == null ? ejemplar.getCodigoQR() == null : e.getCodigoQR().equals(ejemplar.getCodigoQR())){
                    ejemplarRet = e;
                    break;
                }
            }
        }
        return ejemplarRet;
    }

    @Override
    public void actualizarEjemplar(Ejemplar ejemplar) {
        em.merge(ejemplar);
    }
  
}
