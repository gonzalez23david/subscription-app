/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.data;

import UBU.domain.Ejemplar;
import UBU.domain.Publicacion;
import UBU.domain.Suscripcion;
import UBU.domain.Suscriptor;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.*;

/**
 *
 * @author david
 */

@Stateless
public class SuscriptorDaoImpl implements SuscriptorDao {
    
    @PersistenceContext(unitName =  "SubscriptionAppPU")
    EntityManager em;

    @Override
    public List<Suscriptor> encontrarTodosSuscriptores() {
        return em.createNamedQuery("Suscriptor.encontrarTodosSuscriptores").getResultList();
    }

    @Override
    public Suscriptor encontrarSuscriptor(Suscriptor suscriptor) {
        return em.find(Suscriptor.class, suscriptor.getIdSuscriptor());
    }

    @Override
    public void insertarSuscriptor(Suscriptor suscriptor) {
        em.persist(suscriptor);
        em.flush();
    }

    @Override
    public void actualizarSuscriptor(Suscriptor suscriptor) {
        em.merge(suscriptor);
    }

    @Override
    public void eliminarSuscriptor(Suscriptor suscriptor) {
        em.remove(em.merge(suscriptor));
    }

    @Override
    public List<Suscripcion> encontrarTodasSuscripciones(Suscriptor suscriptor) {
        return em.createNamedQuery("Suscriptor.encontrarTodasSuscripciones", Suscripcion.class).setParameter("idSuscriptor", suscriptor).getResultList();
    }

    @Override
    public List<Ejemplar> encontrarTodosEjemplares(Suscriptor suscriptor) {
        List<Ejemplar> ejemplaresAll = em.createNamedQuery("Ejemplar.findAll").getResultList();
        List<Ejemplar> ejemplares = new ArrayList<Ejemplar>();
        for(Ejemplar e : ejemplaresAll){
            if(e.getIdSuscripcion().getIdSuscriptor().equals(suscriptor)){
                ejemplares.add(e);
            }
        }
        return ejemplares;
    }

    @Override
    public List<Ejemplar> encontrarTodosEjemplaresPorPublicacion(Suscriptor suscriptor, Publicacion publicacion) {
        int i = 1;
         List<Ejemplar> ejemplares = encontrarTodosEjemplares(suscriptor);
         List<Ejemplar> ejemplaresPorPublicacion = new ArrayList<Ejemplar>();
         Publicacion publicacionAux;
         System.out.println(ejemplares);
         for(Ejemplar e : ejemplares){
             publicacionAux=e.getIdArticulo().getIdPublicacion();
             if(publicacionAux.equals(publicacion)){
                 ejemplaresPorPublicacion.add(e);
             }
             publicacionAux=null;
         }
         return ejemplaresPorPublicacion;
    }
    
}
