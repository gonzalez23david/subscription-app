/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.data;

import UBU.domain.Editorial;
import UBU.domain.Publicacion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author david
 */

@Stateless
public class PublicacionDaoImpl implements PublicacionDao{
    
    @PersistenceContext(unitName = "SubscriptionAppPU")
    EntityManager em;

    @Override
    public Publicacion encontrarPublicacion(Publicacion publicacion) {
       return em.find(Publicacion.class, publicacion.getIdPublicacion());
    }
    
    @Override
    public Editorial encontrarEditorialPublicacion(Publicacion publicacion) {
         Editorial editorial = encontrarPublicacion(publicacion).getIdEditorial();
         return editorial;
    }
    
    
    
}
