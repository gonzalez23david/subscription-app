/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.data;

import UBU.domain.Ejemplar;
import UBU.domain.Publicacion;
import UBU.domain.Suscripcion;
import UBU.domain.Suscriptor;
import java.util.List;

/**
 *
 * @author david
 */
public interface SuscriptorDao {
    
    public List<Suscriptor> encontrarTodosSuscriptores();
    
    public Suscriptor encontrarSuscriptor(Suscriptor suscriptor);
    
    public void insertarSuscriptor(Suscriptor suscriptor);
    
    public void actualizarSuscriptor(Suscriptor suscriptor);
    
    public void eliminarSuscriptor(Suscriptor suscriptor);
    
    public List<Ejemplar> encontrarTodosEjemplares(Suscriptor suscriptor);
    
    public List<Ejemplar> encontrarTodosEjemplaresPorPublicacion(Suscriptor suscriptor, Publicacion publicacion);
    
    public List<Suscripcion> encontrarTodasSuscripciones(Suscriptor suscriptor);
    
}
