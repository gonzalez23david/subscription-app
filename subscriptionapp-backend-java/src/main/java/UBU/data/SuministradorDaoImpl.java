/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.data;

import UBU.domain.Editorial;
import UBU.domain.Ejemplar;
import UBU.domain.Publicacion;
import UBU.domain.Suministrador;
import UBU.domain.SuministradorEditorial;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author david
 */

@Stateless
public class SuministradorDaoImpl implements SuministradorDao {
    
    @PersistenceContext(unitName = "SubscriptionAppPU")
    EntityManager em;

    @Override
    public List<Suministrador> encontrarTodosSuministradores() {
        return em.createNamedQuery("Suministrador.findAll").getResultList();
    }

    @Override
    public Suministrador encontrarSuministrador(Suministrador suministrador) {
        
        List<Editorial> editoriales = em.createNamedQuery("Suministrador.encontrarTodasEditoriales").setParameter("idSuministrador", suministrador).getResultList();
        List<List<Publicacion>> publicaciones = new ArrayList<>();
        for(Editorial e : editoriales){
            publicaciones.add(em.createNamedQuery("Suministrador.encontrarPublicacionPorEditorial").setParameter("idEditorial", e).getResultList());
        }
        System.out.println("PUBLICACAIONES ENCONTRADAS: "+publicaciones);
        
       return em.find(Suministrador.class, suministrador.getIdSuministrador());
    }

    @Override
    
    public List<Ejemplar> encontrarTodosEjemplares(Suministrador suministrador) {
        return em.createNamedQuery("Suministrador.encontrarTodosEjemplares", Ejemplar.class).setParameter("idSuministrador", suministrador).getResultList();
    }

    @Override
    public void actualizarSuministrador(Suministrador suministrador) {
        em.merge(suministrador);
    }
    
    @Override
    public List<List<Publicacion>> encontrarTodasPublicaciones(Suministrador suministrador) {
        List<Editorial> editoriales = em.createNamedQuery("Suministrador.encontrarTodasEditoriales").setParameter("idSuministrador", suministrador).getResultList();
        List<List<Publicacion>> publicaciones = new ArrayList<>();
        for(Editorial e : editoriales){
            publicaciones.add(em.createNamedQuery("Suministrador.encontrarPublicacionPorEditorial").setParameter("idEditorial", e).getResultList());
        }
       return publicaciones;
    }
   
}
