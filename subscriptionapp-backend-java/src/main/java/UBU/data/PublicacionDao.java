/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.data;

import UBU.domain.Editorial;
import UBU.domain.Publicacion;

/**
 *
 * @author david
 */
public interface PublicacionDao {
    
    public Publicacion encontrarPublicacion(Publicacion publicacion);
    
    public Editorial encontrarEditorialPublicacion(Publicacion publicacion);
    
}
