/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.data;

import UBU.domain.Ejemplar;
import java.util.List;

/**
 *
 * @author david
 */
public interface EjemplarDao {
    
    public List<Ejemplar> encontrarTodosEjemplares();
    
    public Ejemplar encontrarEjemplar(Ejemplar ejemplar);
    
    public Ejemplar encontrarEjemplarPorQR(Ejemplar ejemplar);
    
    public void actualizarEjemplar(Ejemplar ejemplar);
    
}
