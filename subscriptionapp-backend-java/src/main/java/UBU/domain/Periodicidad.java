/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.domain;

import java.io.Serializable;
import java.util.Collection;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author david
 */
@Entity
@Table(name = "periodicidad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Periodicidad.findAll", query = "SELECT p FROM Periodicidad p"),
    @NamedQuery(name = "Periodicidad.findByIdPeriodicidad", query = "SELECT p FROM Periodicidad p WHERE p.idPeriodicidad = :idPeriodicidad"),
    @NamedQuery(name = "Periodicidad.findByNombre", query = "SELECT p FROM Periodicidad p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Periodicidad.findByDescripcion", query = "SELECT p FROM Periodicidad p WHERE p.descripcion = :descripcion")})
public class Periodicidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_periodicidad")
    private Integer idPeriodicidad;
    @Size(max = 20)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 100)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "idPeriodicidad")
    private Collection<Plan> planCollection;

    public Periodicidad() {
    }

    public Periodicidad(Integer idPeriodicidad) {
        this.idPeriodicidad = idPeriodicidad;
    }

    public Integer getIdPeriodicidad() {
        return idPeriodicidad;
    }

    public void setIdPeriodicidad(Integer idPeriodicidad) {
        this.idPeriodicidad = idPeriodicidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    @JsonbTransient
    public Collection<Plan> getPlanCollection() {
        return planCollection;
    }

    public void setPlanCollection(Collection<Plan> planCollection) {
        this.planCollection = planCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPeriodicidad != null ? idPeriodicidad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Periodicidad)) {
            return false;
        }
        Periodicidad other = (Periodicidad) object;
        if ((this.idPeriodicidad == null && other.idPeriodicidad != null) || (this.idPeriodicidad != null && !this.idPeriodicidad.equals(other.idPeriodicidad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UBU.domain.Periodicidad[ idPeriodicidad=" + idPeriodicidad + " ]";
    }
    
}
