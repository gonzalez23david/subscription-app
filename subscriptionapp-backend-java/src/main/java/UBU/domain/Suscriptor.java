/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author david
 */
@Entity
@Table(name = "suscriptor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Suscriptor.findAll", query = "SELECT s FROM Suscriptor s"),
    @NamedQuery(name = "Suscriptor.findByIdSuscriptor", query = "SELECT s FROM Suscriptor s WHERE s.idSuscriptor = :idSuscriptor"),
    @NamedQuery(name = "Suscriptor.findByDni", query = "SELECT s FROM Suscriptor s WHERE s.dni = :dni"),
    @NamedQuery(name = "Suscriptor.findByTelefono", query = "SELECT s FROM Suscriptor s WHERE s.telefono = :telefono"),
    @NamedQuery(name = "Suscriptor.findByNombre", query = "SELECT s FROM Suscriptor s WHERE s.nombre = :nombre"),
    @NamedQuery(name = "Suscriptor.findByApellido1", query = "SELECT s FROM Suscriptor s WHERE s.apellido1 = :apellido1"),
    @NamedQuery(name = "Suscriptor.findByApellido2", query = "SELECT s FROM Suscriptor s WHERE s.apellido2 = :apellido2"),
    @NamedQuery(name = "Suscriptor.findByPassword", query = "SELECT s FROM Suscriptor s WHERE s.password = :password"),
    @NamedQuery(name = "Suscriptor.findByCorreoElectronico", query = "SELECT s FROM Suscriptor s WHERE s.correoElectronico = :correoElectronico"),
    @NamedQuery(name = "Suscriptor.findByFechaNacimiento", query = "SELECT s FROM Suscriptor s WHERE s.fechaNacimiento = :fechaNacimiento"),
    
    @NamedQuery(name = "Suscriptor.encontrarTodosSuscriptores", query = "SELECT s from Suscriptor s ORDER BY s.idSuscriptor"),
    @NamedQuery(name = "Suscriptor.encontrarTodasSuscripciones", query = "SELECT s FROM Suscripcion s WHERE s.idSuscriptor = :idSuscriptor"),
    @NamedQuery(name = "Suscriptor.encontrarEjemplarPorSuscripcion", query = "SELECT e FROM Ejemplar e WHERE e.idSuscripcion = :idSuscripcion")
})
public class Suscriptor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_suscriptor")
    private Integer idSuscriptor;
    @Size(max = 9)
    @Column(name = "DNI")
    private String dni;
    @Column(name = "telefono")
    private Integer telefono;
    @Size(max = 20)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 20)
    @Column(name = "apellido1")
    private String apellido1;
    @Size(max = 20)
    @Column(name = "apellido2")
    private String apellido2;
    @Size(max = 200)
    @Column(name = "password")
    private String password;
    @Size(max = 50)
    @Column(name = "correo_electronico")
    private String correoElectronico;
    
    @Column(name = "fecha_nacimiento")
    //@Temporal(TemporalType.DATE)
    private String fechaNacimiento;
    
    @OneToMany(mappedBy = "idSuscriptor")
    private Collection<Suscripcion> suscripcionCollection;
    

    public Suscriptor() {
    }

    public Suscriptor(Integer idSuscriptor) {
        this.idSuscriptor = idSuscriptor; 
    }

    public Integer getIdSuscriptor() {
        return idSuscriptor;
    }

    public void setIdSuscriptor(Integer idSuscriptor) {
        this.idSuscriptor = idSuscriptor;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @XmlTransient
    @JsonbTransient
    public Collection<Suscripcion> getSuscripcionCollection() {
        return suscripcionCollection;
    }

    public void setSuscripcionCollection(Collection<Suscripcion> suscripcionCollection) {
        this.suscripcionCollection = suscripcionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSuscriptor != null ? idSuscriptor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Suscriptor)) {
            return false;
        }
        Suscriptor other = (Suscriptor) object;
        if ((this.idSuscriptor == null && other.idSuscriptor != null) || (this.idSuscriptor != null && !this.idSuscriptor.equals(other.idSuscriptor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UBU.domain.Suscriptor[ idSuscriptor=" + idSuscriptor + " ] " + fechaNacimiento;
    }
    
    
    
}
