/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author david
 */
@Entity
@Table(name = "ejemplar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ejemplar.findAll", query = "SELECT e FROM Ejemplar e"),
    @NamedQuery(name = "Ejemplar.findByIdEjemplar", query = "SELECT e FROM Ejemplar e WHERE e.idEjemplar = :idEjemplar"),
    @NamedQuery(name = "Ejemplar.findByCodigoQR", query = "SELECT e FROM Ejemplar e WHERE e.codigoQR = :codigoQR"),
    @NamedQuery(name = "Ejemplar.findByLocalizacionSolicitud", query = "SELECT e FROM Ejemplar e WHERE e.localizacionSolicitud = :localizacionSolicitud"),
    @NamedQuery(name = "Ejemplar.findByLocalizacionEntrega", query = "SELECT e FROM Ejemplar e WHERE e.localizacionEntrega = :localizacionEntrega"),
    @NamedQuery(name = "Ejemplar.findByEstado", query = "SELECT e FROM Ejemplar e WHERE e.estado = :estado"),
    @NamedQuery(name = "Ejemplar.findByFechaSolicitud", query = "SELECT e FROM Ejemplar e WHERE e.fechaSolicitud = :fechaSolicitud"),
    @NamedQuery(name = "Ejemplar.findByFechaEntrega", query = "SELECT e FROM Ejemplar e WHERE e.fechaEntrega = :fechaEntrega")})
public class Ejemplar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_ejemplar")
    private Integer idEjemplar;
    @Size(max = 100)
    @Column(name = "codigo_QR")
    private String codigoQR;
    @Size(max = 100)
    @Column(name = "localizacion_solicitud")
    private String localizacionSolicitud;
    @Size(max = 100)
    @Column(name = "localizacion_entrega")
    private String localizacionEntrega;
    @Size(max = 20)
    @Column(name = "estado")
    private String estado;
    @Column(name = "fecha_solicitud")
    //@Temporal(TemporalType.TIMESTAMP) 
    private String fechaSolicitud;
    @Column(name = "fecha_entrega")
    //@Temporal(TemporalType.TIMESTAMP)
    private String fechaEntrega;
    
    @JoinColumn(name = "id_articulo", referencedColumnName = "id_articulo")
    @ManyToOne
    private Articulo idArticulo;
    
    @JoinColumn(name = "id_suministrador", referencedColumnName = "id_suministrador")
    @ManyToOne
    private Suministrador idSuministrador;
    
    @JoinColumn(name = "id_suscripcion", referencedColumnName = "id_suscripcion")
    @ManyToOne
    private Suscripcion idSuscripcion;

    public Ejemplar() {
    }

    public Ejemplar(Integer idEjemplar) {
        this.idEjemplar = idEjemplar;
    }
    
    public Ejemplar(String codigoQR){
        this.codigoQR = codigoQR;
    }

    public Integer getIdEjemplar() {
        return idEjemplar;
    }

    public void setIdEjemplar(Integer idEjemplar) {
        this.idEjemplar = idEjemplar;
    }

    public String getCodigoQR() {
        return codigoQR;
    }

    public void setCodigoQR(String codigoQR) {
        this.codigoQR = codigoQR;
    }

    public String getLocalizacionSolicitud() {
        return localizacionSolicitud;
    }

    public void setLocalizacionSolicitud(String localizacionSolicitud) {
        this.localizacionSolicitud = localizacionSolicitud;
    }

    public String getLocalizacionEntrega() {
        return localizacionEntrega;
    }

    public void setLocalizacionEntrega(String localizacionEntrega) {
        this.localizacionEntrega = localizacionEntrega;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Articulo getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(Articulo idArticulo) {
        this.idArticulo = idArticulo;
    }

    public Suministrador getIdSuministrador() {
        return idSuministrador;
    }

    public void setIdSuministrador(Suministrador idSuministrador) {
        this.idSuministrador = idSuministrador;
    }

    public Suscripcion getIdSuscripcion() {
        return idSuscripcion;
    }

    public void setIdSuscripcion(Suscripcion idSuscripcion) {
        this.idSuscripcion = idSuscripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEjemplar != null ? idEjemplar.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ejemplar)) {
            return false;
        }
        Ejemplar other = (Ejemplar) object;
        if ((this.idEjemplar == null && other.idEjemplar != null) || (this.idEjemplar != null && !this.idEjemplar.equals(other.idEjemplar))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UBU.domain.Ejemplar[ idEjemplar=" + idEjemplar + " ]";
        //return getLocalizacionSolicitud();
    }
    
}
