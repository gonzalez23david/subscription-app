/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author david
 */
@Entity
@Table(name = "suministrador_editorial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SuministradorEditorial.findAll", query = "SELECT s FROM SuministradorEditorial s"),
    @NamedQuery(name = "SuministradorEditorial.findByIdSuministradorEditorial", query = "SELECT s FROM SuministradorEditorial s WHERE s.idSuministradorEditorial = :idSuministradorEditorial")})
public class SuministradorEditorial implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_suministrador_editorial")
    private Integer idSuministradorEditorial;
    @JoinColumn(name = "id_editorial", referencedColumnName = "id_editorial")
    @ManyToOne
    private Editorial idEditorial;
    
    @JoinColumn(name = "id_suministrador", referencedColumnName = "id_suministrador")
    @ManyToOne
    private Suministrador idSuministrador;

    public SuministradorEditorial() {
    }

    public SuministradorEditorial(Integer idSuministradorEditorial) {
        this.idSuministradorEditorial = idSuministradorEditorial;
    }

    public Integer getIdSuministradorEditorial() {
        return idSuministradorEditorial;
    }

    public void setIdSuministradorEditorial(Integer idSuministradorEditorial) {
        this.idSuministradorEditorial = idSuministradorEditorial;
    }

    public Editorial getIdEditorial() {
        return idEditorial;
    }

    public void setIdEditorial(Editorial idEditorial) {
        this.idEditorial = idEditorial;
    }

    public Suministrador getIdSuministrador() {
        return idSuministrador;
    }

    public void setIdSuministrador(Suministrador idSuministrador) {
        this.idSuministrador = idSuministrador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSuministradorEditorial != null ? idSuministradorEditorial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SuministradorEditorial)) {
            return false;
        }
        SuministradorEditorial other = (SuministradorEditorial) object;
        if ((this.idSuministradorEditorial == null && other.idSuministradorEditorial != null) || (this.idSuministradorEditorial != null && !this.idSuministradorEditorial.equals(other.idSuministradorEditorial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UBU.domain.SuministradorEditorial[ idSuministradorEditorial=" + idSuministradorEditorial + " ]";
    }
    
}
