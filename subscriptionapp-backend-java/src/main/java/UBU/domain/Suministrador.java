/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author david
 */
@Entity
@Table(name = "suministrador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Suministrador.findAll", query = "SELECT s FROM Suministrador s"),
    @NamedQuery(name = "Suministrador.findByIdSuministrador", query = "SELECT s FROM Suministrador s WHERE s.idSuministrador = :idSuministrador"),
    @NamedQuery(name = "Suministrador.findByDni", query = "SELECT s FROM Suministrador s WHERE s.dni = :dni"),
    @NamedQuery(name = "Suministrador.findByTelefono", query = "SELECT s FROM Suministrador s WHERE s.telefono = :telefono"),
    @NamedQuery(name = "Suministrador.findByNombre", query = "SELECT s FROM Suministrador s WHERE s.nombre = :nombre"),
    @NamedQuery(name = "Suministrador.findByApellido1", query = "SELECT s FROM Suministrador s WHERE s.apellido1 = :apellido1"),
    @NamedQuery(name = "Suministrador.findByApellido2", query = "SELECT s FROM Suministrador s WHERE s.apellido2 = :apellido2"),
    @NamedQuery(name = "Suministrador.findByPassword", query = "SELECT s FROM Suministrador s WHERE s.password = :password"),
    @NamedQuery(name = "Suministrador.findByCorreoElectronico", query = "SELECT s FROM Suministrador s WHERE s.correoElectronico = :correoElectronico"),
    @NamedQuery(name = "Suministrador.findByFechaNacimiento", query = "SELECT s FROM Suministrador s WHERE s.fechaNacimiento = :fechaNacimiento"),
    @NamedQuery(name = "Suministrador.findByCuentaBancaria", query = "SELECT s FROM Suministrador s WHERE s.cuentaBancaria = :cuentaBancaria"),
    @NamedQuery(name = "Suministrador.findByDireccion", query = "SELECT s FROM Suministrador s WHERE s.direccion = :direccion"),
    @NamedQuery(name = "Suministrador.findByPais", query = "SELECT s FROM Suministrador s WHERE s.pais = :pais"),
    @NamedQuery(name = "Suministrador.findByProvincia", query = "SELECT s FROM Suministrador s WHERE s.provincia = :provincia"),
    @NamedQuery(name = "Suministrador.findByCp", query = "SELECT s FROM Suministrador s WHERE s.cp = :cp"),
    @NamedQuery(name = "Suministrador.findByLocalidad", query = "SELECT s FROM Suministrador s WHERE s.localidad = :localidad"),
    
    @NamedQuery(name = "Suministrador.encontrarTodosEjemplares", query = "SELECT e FROM Ejemplar e WHERE e.idSuministrador = :idSuministrador"),
    @NamedQuery(name = "Suministrador.encontrarTodasEditoriales", query = "SELECT su.idEditorial FROM SuministradorEditorial su WHERE su.idSuministrador = :idSuministrador"),
    @NamedQuery(name = "Suministrador.encontrarPublicacionPorEditorial", query = "SELECT p FROM Publicacion p WHERE p.idEditorial = :idEditorial")
})
public class Suministrador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_suministrador")
    private Integer idSuministrador;
    @Size(max = 9)
    @Column(name = "DNI")
    private String dni;
    @Column(name = "telefono")
    private Integer telefono;
    @Size(max = 20)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 20)
    @Column(name = "apellido1")
    private String apellido1;
    @Size(max = 20)
    @Column(name = "apellido2")
    private String apellido2;
    @Size(max = 200)
    @Column(name = "password")
    private String password;
    @Size(max = 50)
    @Column(name = "correo_electronico")
    private String correoElectronico;
    
    @Column(name = "fecha_nacimiento")
    //@Temporal(TemporalType.DATE)
    private String fechaNacimiento;
    
    @Size(max = 20)
    @Column(name = "cuenta_bancaria")
    private String cuentaBancaria;
    @Size(max = 30)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 30)
    @Column(name = "pais")
    private String pais;
    @Size(max = 30)
    @Column(name = "provincia")
    private String provincia;
    @Column(name = "cp")
    private String cp;
    @Size(max = 30)
    @Column(name = "localidad")
    private String localidad;
    @OneToMany(mappedBy = "idSuministrador")
    private Collection<Ejemplar> ejemplarCollection;
    
    @OneToMany(mappedBy = "idSuministrador")
    private List<SuministradorEditorial> suministradorEditorialCollection;

    public Suministrador() {
    }

    public Suministrador(Integer idSuministrador) {
        this.idSuministrador = idSuministrador;
    }

    public Integer getIdSuministrador() {
        return idSuministrador;
    }

    public void setIdSuministrador(Integer idSuministrador) {
        this.idSuministrador = idSuministrador;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCuentaBancaria() {
        return cuentaBancaria;
    }

    public void setCuentaBancaria(String cuentaBancaria) {
        this.cuentaBancaria = cuentaBancaria;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    @XmlTransient
    @JsonbTransient
    public Collection<Ejemplar> getEjemplarCollection() {
        return ejemplarCollection;
    }

    public void setEjemplarCollection(Collection<Ejemplar> ejemplarCollection) {
        this.ejemplarCollection = ejemplarCollection;
    }

    @XmlTransient
    @JsonbTransient
    public List<SuministradorEditorial> getSuministradorEditorialCollection() {
        return suministradorEditorialCollection;
    }

    public void setSuministradorEditorialCollection(List<SuministradorEditorial> suministradorEditorialCollection) {
        this.suministradorEditorialCollection = suministradorEditorialCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSuministrador != null ? idSuministrador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Suministrador)) {
            return false;
        }
        Suministrador other = (Suministrador) object;
        if ((this.idSuministrador == null && other.idSuministrador != null) || (this.idSuministrador != null && !this.idSuministrador.equals(other.idSuministrador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UBU.domain.Suministrador[ idSuministrador=" + idSuministrador + " ]";
    }
    
}
