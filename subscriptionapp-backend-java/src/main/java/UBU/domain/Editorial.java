/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UBU.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author david
 */
@Entity
@Table(name = "editorial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Editorial.findAll", query = "SELECT e FROM Editorial e"),
    @NamedQuery(name = "Editorial.findByIdEditorial", query = "SELECT e FROM Editorial e WHERE e.idEditorial = :idEditorial"),
    @NamedQuery(name = "Editorial.findByNif", query = "SELECT e FROM Editorial e WHERE e.nif = :nif"),
    @NamedQuery(name = "Editorial.findByNombreEditorial", query = "SELECT e FROM Editorial e WHERE e.nombreEditorial = :nombreEditorial"),
    @NamedQuery(name = "Editorial.findByTelefono", query = "SELECT e FROM Editorial e WHERE e.telefono = :telefono"),
    @NamedQuery(name = "Editorial.findByDireccion", query = "SELECT e FROM Editorial e WHERE e.direccion = :direccion"),
    @NamedQuery(name = "Editorial.findByPais", query = "SELECT e FROM Editorial e WHERE e.pais = :pais"),
    @NamedQuery(name = "Editorial.findByProvincia", query = "SELECT e FROM Editorial e WHERE e.provincia = :provincia"),
    @NamedQuery(name = "Editorial.findByCp", query = "SELECT e FROM Editorial e WHERE e.cp = :cp"),
    @NamedQuery(name = "Editorial.findByLocalidad", query = "SELECT e FROM Editorial e WHERE e.localidad = :localidad"),
    @NamedQuery(name = "Editorial.findByContacto", query = "SELECT e FROM Editorial e WHERE e.contacto = :contacto"),

    @NamedQuery(name = "Editorial.encontrarTodosSuministradores", query = "SELECT se.idSuministrador FROM SuministradorEditorial se WHERE se.idEditorial = :idEditorial"),
    @NamedQuery(name = "Editorial.encontrarEjemplarPorSuministrador", query = "SELECT e FROM Ejemplar e WHERE e.idSuministrador = :idSuministrador")
})
public class Editorial implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_editorial")
    private Integer idEditorial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "NIF")
    private String nif;
    @Size(max = 20)
    @Column(name = "nombre_editorial")
    private String nombreEditorial;
    @Column(name = "telefono")
    private Integer telefono;
    @Size(max = 30)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 30)
    @Column(name = "pais")
    private String pais;
    @Size(max = 30)
    @Column(name = "provincia")
    private String provincia;
    @Column(name = "cp")
    private Integer cp;
    @Size(max = 30)
    @Column(name = "localidad")
    private String localidad;
    @Size(max = 30)
    @Column(name = "contacto")
    private String contacto;
    @OneToMany(mappedBy = "idEditorial")
    private Collection<SuministradorEditorial> suministradorEditorialCollection;
    @OneToMany(mappedBy = "idEditorial")
    private List<Publicacion> publicacionCollection;

    public Editorial() {
    }

    public Editorial(Integer idEditorial) {
        this.idEditorial = idEditorial;
    }

    public Editorial(Integer idEditorial, String nif) {
        this.idEditorial = idEditorial;
        this.nif = nif;
    }

    public Integer getIdEditorial() {
        return idEditorial;
    }

    public void setIdEditorial(Integer idEditorial) {
        this.idEditorial = idEditorial;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNombreEditorial() {
        return nombreEditorial;
    }

    public void setNombreEditorial(String nombreEditorial) {
        this.nombreEditorial = nombreEditorial;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public Integer getCp() {
        return cp;
    }

    public void setCp(Integer cp) {
        this.cp = cp;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    @XmlTransient
    @JsonbTransient
    public Collection<SuministradorEditorial> getSuministradorEditorialCollection() {
        return suministradorEditorialCollection;
    }

    public void setSuministradorEditorialCollection(Collection<SuministradorEditorial> suministradorEditorialCollection) {
        this.suministradorEditorialCollection = suministradorEditorialCollection;
    }

    @XmlTransient
    @JsonbTransient
    public List<Publicacion> getPublicacionCollection() {
        return publicacionCollection;
    }

    public void setPublicacionCollection(List<Publicacion> publicacionCollection) {
        this.publicacionCollection = publicacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEditorial != null ? idEditorial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Editorial)) {
            return false;
        }
        Editorial other = (Editorial) object;
        if ((this.idEditorial == null && other.idEditorial != null) || (this.idEditorial != null && !this.idEditorial.equals(other.idEditorial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UBU.domain.Editorial[ idEditorial=" + idEditorial + " ]";
    }
    
}
