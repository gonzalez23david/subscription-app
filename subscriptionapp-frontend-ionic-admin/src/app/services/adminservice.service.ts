import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {

  ip = "192.168.1.132";
  urlEjemplares = "http://"+this.ip+":8080/subscriptionapp-backend-java/subapp/ejemplares/";

  httpHeaders = {
    headers: new HttpHeaders({'Accept' : 'application/json','Content-Type' : 'application/json'})
  };

  constructor(private http : HttpClient) { }

  cargarTodosEjemplares(){
    return this.http.get(this.urlEjemplares);
    //return this.http.get("http://localhost:8080/subscriptionapp-backend-java/subapp/ejemplares/");
  }

  cargarEjemplar(id:string){
    console.log(id);
    //return this.http.get(this.urlEjemplares+id);
    return this.http.get("http://localhost:8080/subscriptionapp-backend-java/subapp/ejemplares/"+id);
  }
}
