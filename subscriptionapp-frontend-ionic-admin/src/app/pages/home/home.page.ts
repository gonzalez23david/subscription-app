import { Component, OnInit } from '@angular/core';
import { AdminserviceService } from 'src/app/services/adminservice.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  ejemplares = null;
  value="";

  tableStyle = 'bootstrap';

  escuchaActiva : boolean = false;
  color = "danger";

  constructor(private adminService : AdminserviceService, private router : Router) { }

  ngOnInit() {
    this.cargarTodosEjemplares();
  }

  cargarTodosEjemplares(){
    this.adminService.cargarTodosEjemplares().subscribe(result=>{
      this.ejemplares=result;
    });
  }

  async cargarEjemplar(id){
    console.log(id);
    this.router.navigate(['/inicio', id.idEjemplar]);
  }

  actualizar(){
    this.cargarTodosEjemplares();
  }

  async escuchar(){
    if(this.escuchaActiva==false){
      this.escuchaActiva=true;
      this.color="success"
    }else{
      this.escuchaActiva=false;
      this.color="danger"
    }
    while(this.escuchaActiva==true){
      this.cargarTodosEjemplares();
      await this.delay(1000);
    }

  }

  private delay(ms : number){
    return new Promise(resolve => setTimeout(resolve,ms));
  }

}
