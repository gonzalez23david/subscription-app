import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import {Router} from '@angular/router'
import { AdminserviceService } from 'src/app/services/adminservice.service'

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  argumento = null;
  ejemplar = null;
  nombre = "";
  QR = "";
  id = "";
  nombrePublicacion = "";
  idPublicacion = "";
  nombreEditorial = "";
  idEditorial = "";
  nombreSuscriptor = "";
  idSuscriptor = "";


  constructor(private router : Router, private activatedRoute : ActivatedRoute, private adminService : AdminserviceService) { }

  ngOnInit() {
    this.argumento=this.activatedRoute.snapshot.paramMap.get("id");
    console.log(this.argumento);
    this.cargarEjemplar(this.argumento);
    console.log(this.ejemplar);
  }

  cargarEjemplar(id){
    this.adminService.cargarEjemplar(this.argumento).subscribe(result=>{
      this.ejemplar=result;
      this.nombre=this.ejemplar.idArticulo.nombre;
      this.QR=this.ejemplar.codigoQR;
      this.nombrePublicacion =this.ejemplar.idArticulo.idPublicacion.nombre;
      this.idPublicacion = this.ejemplar.idArticulo.idPublicacion.idPublicacion;
      this.nombreEditorial = this.ejemplar.idArticulo.idPublicacion.idEditorial.nombreEditorial;
      this.idEditorial = this.ejemplar.idArticulo.idPublicacion.idEditorial.idEditorial;
      this.nombreSuscriptor =this.ejemplar.idSuscripcion.idSuscriptor.nombre+" "
                            +this.ejemplar.idSuscripcion.idSuscriptor.apellido1+" "
                            +this.ejemplar.idSuscripcion.idSuscriptor.apellido2;
      this.idSuscriptor = this.ejemplar.idSuscripcion.idSuscriptor.idSuscriptor;
    });
  }

}
