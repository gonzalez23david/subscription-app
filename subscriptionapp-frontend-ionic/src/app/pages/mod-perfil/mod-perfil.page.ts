import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { PerfilService } from 'src/app/services/perfil.service'

@Component({
  selector: 'app-mod-perfil',
  templateUrl: './mod-perfil.page.html',
  styleUrls: ['./mod-perfil.page.scss'],
})
export class ModPerfilPage implements OnInit {

  argumento : string = null;
  perfil = null;
  colorLetra : string = "";

  nombre = "";
  nnombre = "";
  ap1 = "";
  nap1 = "";
  ap2   = "";
  nap2 = "";
  dni = "";
  ndni = "";
  email = "";
  nemail = "";
  fnac = "";
  nfnac = "";
  tfno = "";
  ntfno = "";
  constructor(private router : Router, private activatedRoute : ActivatedRoute, private perfilService : PerfilService) { }

  ngOnInit() {
    this.argumento = this.activatedRoute.snapshot.paramMap.get("id");
    this.cargarPerfil(this.argumento);
  }

  cargarPerfil(id){
    this.perfilService.consultarPerfil(id).subscribe(result =>{
      this.perfil = result;
      this.nombre = this.perfil.nombre;
      this.ap1 = this.perfil.apellido1;
      this.ap2 = this.perfil.apellido2;
      this.dni = this.perfil.dni;
      this.email = this.perfil.correoElectronico;
      this.fnac = this.perfil.fechaNacimiento;
      this.tfno = this.perfil.telefono;
    });
  }

  realizarCambios(){
    //Meter confimación de que al menos un campo este rellenado.
    var perfilAux = this.perfil;
    var bool : boolean = false;
    if(this.nnombre != ""){
      perfilAux.nombre = this.nnombre;
      bool = true;
    }
    if(this.nap1 != ""){
      perfilAux.apellido1 = this.nap1;
      bool = true;
    }
    if(this.nap2 != ""){
      perfilAux.apellido2 = this.nap2;
      bool = true;
    }
    if(this.ndni != ""){
      perfilAux.dni = this.ndni;
      bool = true;
    }
    if(this.nemail != ""){
      perfilAux.correoElectronico = this.nemail;
      bool = true;
    }
    if(this.nfnac != ""){
      perfilAux.fechaNacimiento = this.nfnac;
      bool = true;
    }
    if(this.ntfno != ""){
      perfilAux.telefono = this.ntfno;
      bool = true;
    }
    
    if(bool == true){
      this.perfilService.modificarPerfil(perfilAux);
      this.colorLetra = "success"
      document.getElementById("conf").innerHTML="Modificación realizada"
      //this.router.navigate(['/perfil', this.perfil.idSuscriptor]);
    }else{
      this.colorLetra = "danger"
      document.getElementById("conf").innerHTML="No hay ningun cambio"
    }
    
  }

}
