import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PerfilService } from 'src/app/services/perfil.service';
import { stringify } from 'querystring';

@Component({
  selector: 'app-publicacion',
  templateUrl: './publicacion.page.html',
  styleUrls: ['./publicacion.page.scss'],
})
export class PublicacionPage implements OnInit {

  idSuscriptor : string;
  idPublicacion : string;
  nombrePublicacion : string;
  informationPublicacion = null;
  inforamtionEjemplares = null;

  constructor(private activatedRoute : ActivatedRoute, private perfilService : PerfilService, private router : Router) { }

  ngOnInit() {
    this.idSuscriptor=this.activatedRoute.snapshot.paramMap.get("idS");
    this.idPublicacion=this.activatedRoute.snapshot.paramMap.get("idP");
    this.cargarPublicacion(this.idPublicacion);
    this.cargarEjemplares(this.idSuscriptor,this.idPublicacion);
  }

  ionViewDidEnter(){
    this.cargarPublicacion(this.idPublicacion);
    this.cargarEjemplares(this.idSuscriptor,this.idPublicacion);
  }

  cargarPublicacion(idPublicacion:string){
    this.perfilService.consultarPublicacion(idPublicacion).subscribe(result => {
      this.informationPublicacion = result;
      this.nombrePublicacion = this.informationPublicacion.nombre;
      console.log("Publicacion accedida:")
      console.log(this.informationPublicacion);
    });
  }

  cargarEjemplares(idSuscriptor:string, idPublicacion:string){
      this.perfilService.consultarEjemplaresPorPublicacion(idSuscriptor, idPublicacion).subscribe(result =>{
        this.inforamtionEjemplares = result;
        console.log("Ejemplares cargados:")
        console.log(this.inforamtionEjemplares);
      })
  }

  navegarEjemplar(idE:string){
    this.router.navigate(['/entrega',idE]);
  }

}
