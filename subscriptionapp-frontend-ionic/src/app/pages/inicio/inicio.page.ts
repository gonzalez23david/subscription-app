import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import {Router} from '@angular/router'
import { PerfilService } from 'src/app/services/perfil.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  argumento : string = null;
  information = null;
  cero : string = "0";
  i = null;
  suscripciones : Object[];
  nombre : string = "";

  constructor(private activatedRoute : ActivatedRoute, 
              private router : Router,
              private perfilService : PerfilService) { }

  ngOnInit() {
    this.argumento = this.activatedRoute.snapshot.paramMap.get("id");
    this.cargarSuscripciones(this.argumento);
  }

  ionViewDidEnter(){
    this.cargarSuscripciones(this.argumento);
  }

  verPerfil(){
    this.router.navigate(['/perfil', this.argumento])
  }

  cargarSuscripciones(id:string){
    this.perfilService.consultarSuscripciones(id).subscribe(result =>{
      this.information=result;
      this.nombre = this.information[0].idSuscriptor.nombre;
      console.log("Bucle FOR");
      for (this.i in this.information){
        console.log(this.information[this.i]);
      }
    });   
    
  }

  cargarPublicacion(idPublicacion:any){
    this.router.navigate(['/publicacion',this.argumento,idPublicacion]);
  }
  

}
