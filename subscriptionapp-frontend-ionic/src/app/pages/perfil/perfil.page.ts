import { PerfilService } from './../../services/perfil.service'
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  information = null;
  argumento : string = null; //Es el id que le pasamos por parametro en la pagina home

  constructor(private activatedRoute : ActivatedRoute, private perfilService : PerfilService, private router : Router) { }

  ngOnInit() {
    this.argumento = this.activatedRoute.snapshot.paramMap.get("id");//entre comillas va el valor que pusimos en app-routing
    this.cargarPerfil(this.argumento);
  }

  ionViewDidEnter(){
    this.cargarPerfil(this.argumento);
  }

  editarPerfil(){
    this.router.navigate(['mod-perfil', this.argumento]);
  }
  
  cargarPerfil(id:string){
      this.perfilService.consultarPerfil(id).subscribe(result => {
      this.information = result;
      console.log("Datos obtenidos:");
      console.log(this.information);
      document.getElementById("nombre").innerHTML =this.information.nombre;
      document.getElementById("apellido1").innerHTML =this.information.apellido1;
      document.getElementById("apellido2").innerHTML =this.information.apellido2;
      document.getElementById("dni").innerHTML =this.information.dni;
      document.getElementById("correo").innerHTML =this.information.correoElectronico;
      document.getElementById("fechaN").innerHTML =this.information.fechaNacimiento;
      document.getElementById("telefono").innerHTML =this.information.telefono;
    });
  }
}
