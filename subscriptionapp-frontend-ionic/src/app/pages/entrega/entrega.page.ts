import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PerfilService } from 'src/app/services/perfil.service';
import {Md5} from 'ts-md5/dist/md5';
import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-entrega',
  templateUrl: './entrega.page.html',
  styleUrls: ['./entrega.page.scss'],
})
export class EntregaPage implements OnInit {

  options : GeolocationOptions;

  ejemplar = null;
  ejemplarConf = null;
  qrData = "";
  elementType : 'url' | 'string' = 'url';
  nombreEjemplar : string ;
  cadenaQR : string;
  hashQR : string;

  //Variables utilizadas para cambiar el color del texto
  colorito : string;
  color:string = "danger";

  //variable que va a contener los mensajes que se muestran por pantalla
  texto1 : string;
  texto2 : string;

  //Campos para generar el string del QR
  idEjemplar : string;
  idArticulo : string;
  idPublicacion : string;
  issnPublicacion : string;
  idEditorial : string;
  nifEditorial : string;
  estado: string;
  ahora : Number; //Variable que guarda el momento en el que se genera el QR

  //Muestra los botones para confirmar y rechazar
  ocultar : boolean = false;
  //Condiciones de salida del metodo Refrescar
  //Condicion cuando el proceso llega al estado final(Confirmado)
  finTrans : boolean = false;
  //Condicion cuando se cancela y el estado es el inicial(Disponible)
  inicioTrans : boolean = false;
  //Muestra si el metodo refrescar esta activo
  metActivo : boolean = false;

  //Variable contador del tiempo restante para que el codigo caduque
  cuentaAtras = 58;
  //Boolean para mostrar el contador
  ocultarCuentaAtras=false;

  //Boolean para saber cuando finaliza la transacción.
  //El objetivo es quitar tods y obligar al usuario a ir para atras.
  finalizado : boolean = true;
  finalizadoBtn : boolean = false;

  constructor(private activatedRoute : ActivatedRoute, private perfilService : PerfilService, private geolocation : Geolocation) {}
              

  //Metodo que se ejecuta al cargar la vista
  ngOnInit() {
    this.idEjemplar=this.activatedRoute.snapshot.paramMap.get("idE");
      this.cargarEjemplar(this.idEjemplar);
  }

  //Método que se ejecuta al abandonar la vista
  ionViewWillLeave(){
    this.finTrans=true;
  }

  //Metodo que recibe el ejemplar y asigna los valores recibidos
  cargarEjemplar(id:String){
    this.perfilService.consultarEjemplar(id).subscribe(result =>{
      this.ejemplar = result;
      this.estado = this.ejemplar.estado;
      this.nombreEjemplar = this.ejemplar.idArticulo.nombre;
      this.idArticulo = this.ejemplar.idArticulo.idArticulo;
      this.idPublicacion = this.ejemplar.idArticulo.idPublicacion.idPublicacion;
      this.issnPublicacion = this.ejemplar.idArticulo.idPublicacion.issn;
      this.idEditorial = this.ejemplar.idArticulo.idPublicacion.idEditorial.idEditorial;
      this.nifEditorial = this.ejemplar.idArticulo.idPublicacion.idEditorial.nif;
      this.qrData = this.ejemplar.codigoQR;
    });
  }


  //Metodo que crea un código QR recogiendo los datos necesarios y realizando los cambios determinados
  async generarQR(){
    console.log("Estado del Ejemplar: "+this.ejemplar.estado)
    //Comprobar que el estado es Disponible para generar el QR
    if(this.ejemplar.estado == "Disponible"){
      
      this.ahora = Date.now();
      console.log("Ahora: "+this.ahora);

      this.cadenaQR= this.ahora+this.idEjemplar+this.idArticulo+this.idPublicacion+this.issnPublicacion+this.idEditorial+this.nifEditorial;
      
      console.log("Cadena QR: "+this.cadenaQR);
      this.hashQR = Md5.hashStr(this.cadenaQR).toString();
      this.qrData = this.hashQR;

      //Cuando se solicita un ejemplar se genera: Código QR, localizacion_solicitud, fecha_solicitud y cambio de estado
      var ejemplarAux = this.ejemplar;
      ejemplarAux.codigoQR = this.qrData;

      //Obtenemos el momento de la solicitud
      var fecha_solicitud = new Date();
      var hora_solicitud = fecha_solicitud.getHours() + ":" + fecha_solicitud.getMinutes() + ":" + fecha_solicitud.getSeconds();
      console.log("Fecha Solicitud: "+hora_solicitud.toString());
      ejemplarAux.fechaSolicitud = hora_solicitud.toString();
      
      //Obtenemos el lugar de la solicitud
     ejemplarAux.localizacionSolicitud=null;
      this.options = {enableHighAccuracy : true};
      this.geolocation.getCurrentPosition().then((geoposition : Geoposition) => {
        var lat = geoposition.coords.latitude;
        var lng = geoposition.coords.longitude;
        ejemplarAux.localizacionSolicitud = lat+", "+lng;
      });

      //Espera para que le de tiempo a guardar la localización
      while(ejemplarAux.localizacionSolicitud==null){
        await this.delay(100);
        console.log("Espera");
      }

      ejemplarAux.estado = "Solicitado";
      

      //Lanzamos la peticion con los datos grabados
      this.perfilService.cargarEjemplarQR(ejemplarAux);

      //Metodo en bucle para actulizar el estado
      //No importa que se lance aqui un bucle porque en principio el QR solo se genera una vez cuando el estado es Disponible
      this.refrescarEstado();
      
    }
  }

  //Metodo asociado al boton CONFIRMAR
  confirmarQR(){
    var ejemplarAux = this.ejemplar;
    ejemplarAux.estado = "Confirmado";
    this.perfilService.cargarEjemplarQR(ejemplarAux);
  }

  //Metodo asociado al boton RECHAZAR
  rechazarQR(){
    var ejemplarAux = this.ejemplar;
    ejemplarAux.estado = "A Disposicion";
    this.perfilService.cargarEjemplarQR(ejemplarAux);
    this.colorito = "danger";
    this.texto1="Ejemplar Rechazado.";
    this.texto2="";
  }

  //Metodo que se ejecuta cuando se inicia una transacción. Se ejecuta hasta que finaliza la transacción.
  //Esta constatementa lanzando peticiones al servidor para actualizar la información del Ejemplar
  async refrescarEstado(){
    this.metActivo = true;
    this.color="success";
    while(this.finTrans == false){
      console.log("Estado (refrescar) = "+this.estado);
      this.perfilService.consultarEjemplar(this.idEjemplar).subscribe(async result =>{
        this.ejemplar = result;
        this.estado = this.ejemplar.estado;
        //Comprueba si el QR ha sido leido exitosamente por el Suministrador
        if(this.estado == "A Disposicion" && this.inicioTrans == false){
          this.inicioTrans = true;
        }
        //Este caso es cuando se cancela la transacción.
        if(this.estado == "Disponible" && this.inicioTrans == true){
          this.colorito = "danger"
          this.texto1="Ejemplar No Disponible.";
          this.texto2="Transacción Insatisfactoria.";
          this.metActivo = false;
          this.color="danger";
          this.finTrans = true;
          this.finalizado=false;
          this.finalizadoBtn=true;
        }
        //Si el estado es Entregado muestra los botones de CONFIRMAR y RECHAZAR
        if(this.estado == "Entregado"){
          this.colorito = "success";
          this.texto1="Ejemplar Recibido.";
          this.texto2="";
          this.ocultar=true;
        }else{
          this.ocultar=false;
        }
        //Haciendo pruebas si confirmamos el ejemplar sale de aqui y el estado no se actualiza
        if(this.estado == "Confirmado"){
          this.colorito = "success";
          this.texto1="Ejemplar Confirmado.";
          this.texto2="Transacción Satisfactoria.";
          this.metActivo = false;
          this.color="danger";
          this.finTrans=true;
        }
        //Comprobamos si el QR ha caducado. En caso afirmativo la transacción finaliza.
        if(this.comprobarHora(this.ejemplar.fechaSolicitud)==false){
          //Si el QR caduca y el estado es Entregado se pasa a Confirmado (Diseño de la comunicación)
          if(this.estado=="Entregado"){
            var ejemplarAuxFalse = this.ejemplar;
            ejemplarAuxFalse.estado = "Confirmado"
            this.estado="Confirmado";
            this.colorito="danger"
            this.color="danger"
            this.texto1="Código Caducado.";
            this.texto2="Transacción Finalizada.";
            this.perfilService.cargarEjemplarQR(ejemplarAuxFalse);
            this.metActivo = false;
            this.finTrans=true;
            this.ocultar=false;
          }else{
            var ejemplarAuxFalse = this.ejemplar;
            ejemplarAuxFalse.codigoQR = null;
            ejemplarAuxFalse.fechaSolicitud = null;
            ejemplarAuxFalse.fechaEntrega = null;
            ejemplarAuxFalse.localizacionSolicitud = null;
            ejemplarAuxFalse.localizacionEntrega = null;
            ejemplarAuxFalse.idSuministrador = null;
            ejemplarAuxFalse.estado = "Disponible";
            this.estado="Disponible"
            this.colorito="danger"
            this.texto1="Código Caducado.";
            this.texto2="";
            this.perfilService.cargarEjemplarQR(ejemplarAuxFalse);
            this.metActivo = false;
            this.color="danger";
            this.finTrans=true;
            this.ocultar=false;
            this.finalizado=false;
            this.finalizadoBtn=true;
          }
        }
      });
      await this.delay(1000);
    }
  }

  private delay(ms : number){
    return new Promise(resolve => setTimeout(resolve,ms));
  }

  //Metodo que comprueba que el codigo no ha caduca
  comprobarHora(inicio){
    this.ocultarCuentaAtras=true;
    var booleanHora : boolean = true;

    var array = inicio.split(":");
    var hI = parseInt(array[0]);
    var mI = parseInt(array[1]);
    var sI = parseInt(array[2]);

    var ahora = new Date();
    var hA = ahora.getHours();
    var mA = ahora.getMinutes();
    var sA = ahora.getSeconds();

    console.log("Inicio= "+sI);
    console.log("Ahora= "+sA);
    this.cuentaAtras=this.cuentaAtras-1;

    if(sI == sA){
      booleanHora=false;
      this.ocultarCuentaAtras=false;
    }
    return booleanHora;
  }
}
 




