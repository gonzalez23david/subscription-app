import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import { PerfilService } from 'src/app/services/perfil.service';
import {Md5} from 'ts-md5/dist/md5';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  id = "";
  pwd = "";
  error = "";
  information = null;
  ocultar : boolean = false;

  constructor(private router : Router, private perfilService : PerfilService) { }

  ngOnInit() {
    document.getElementById("err").innerHTML =" ";
  }

  navegarPagina(){
    //this.router.navigate(['/perfil', this.id]);
    var hash = Md5.hashStr("admin");
    document.getElementById("err").innerHTML = hash.toString();
  }

  funcAux(){
    this.comprobarUsuario(this.id);
  }

  comprobarUsuario(id:string){
    document.getElementById("err").innerHTML ="";
    this.ocultar=true;
    this.perfilService.consultarPerfil(id).subscribe(result=>{
      this.information=result;
      this.ocultar=false;
      var hashPwd = Md5.hashStr(this.pwd);
      console.log(hashPwd);
      console.log(this.information.password);
      if(this.information.password == hashPwd){
        this.router.navigate(['/inicio', this.id]);
      }else{
        document.getElementById("err").innerHTML ="Contraseña Incorrecta";
      }
    }, error =>{
      this.ocultar=false;
      document.getElementById("err").innerHTML ="La IP no es correcta";
    });
    console.log("Final del metodo");
    
  
  }

  presentAlertPrompt() {
    const alert = document.createElement('ion-alert');
    alert.header = 'Introduce la IP';
    alert.inputs = [
      {
        name: 'name2',
        id: 'name2-id',
        placeholder: this.perfilService.getIP()
      }
    ];
    alert.buttons = [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm Cancel')
        }
      }, {
        text: 'Ok',
        handler: (alertData) => {
          if(alertData.name2 != ""){
            console.log("Nueva IP: "+alertData.name2);
            this.perfilService.setIP(alertData.name2);
          }
        }
      }
    ];
  
    document.body.appendChild(alert);
    return alert.present();
  }

}
