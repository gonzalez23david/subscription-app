import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  //{ path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'home',   loadChildren: './pages/home/home.module#HomePageModule'},
  { path: 'perfil/:id', loadChildren: './pages/perfil/perfil.module#PerfilPageModule' },
  { path: 'inicio/:id', loadChildren: './pages/inicio/inicio.module#InicioPageModule' },
  { path: 'publicacion/:idS/:idP', loadChildren: './pages/publicacion/publicacion.module#PublicacionPageModule' },
  { path: 'entrega/:idE', loadChildren: './pages/entrega/entrega.module#EntregaPageModule' },
  { path: 'mod-perfil/:id', loadChildren: './pages/mod-perfil/mod-perfil.module#ModPerfilPageModule' },



];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
