import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class PerfilService {
  //url = 'http://localhost:8080/subscriptionapp-backend-java/subapp/suscriptores/3/';
  ip = "192.168.1.128"; 
  urlSuscriptores = 'http://'+this.ip+':8080/subscriptionapp-backend-java/subapp/suscriptores/';
  urlPublicaciones = 'http://'+this.ip+':8080/subscriptionapp-backend-java/subapp/publicaciones/';
  urlEjemplares = "http://"+this.ip+":8080/subscriptionapp-backend-java/subapp/ejemplares/";
  suscripciones = "/scrnes";
  ejemplares = "/ejem"
  barra = "/";
  apiKey = '';
  json=".json";
  

  httpHeaders = {
    headers: new HttpHeaders({'Accept' : 'application/json','Content-Type' : 'application/json'})
  };

  constructor(private http : HttpClient) {  }

  setIP(ip){
    this.ip = ip;
    console.log("Nueva IP servicio: "+this.ip);
    this.urlSuscriptores = 'http://'+this.ip+':8080/subscriptionapp-backend-java/subapp/suscriptores/';
    this.urlPublicaciones = 'http://'+this.ip+':8080/subscriptionapp-backend-java/subapp/publicaciones/';
    this.urlEjemplares = "http://"+this.ip+":8080/subscriptionapp-backend-java/subapp/ejemplares/";
  }

  getIP(){
    return this.ip;
  }

  cargarEjemplarQR(ejemplar){
    console.log("Ejemplar a modificar: ");
    console.log(ejemplar);
    this.http.put(this.urlEjemplares+ejemplar.idEjemplar,ejemplar,this.httpHeaders).subscribe(
      data => {
        console.log("PUT bien:");
        console.log(data['_body']);
      }, error =>{
        console.log("PUT mal:");
        console.log(error);
      }
    );
  }

  modificarPerfil(perfil){
    console.log("Perfil a modificar: "+perfil);
    this.http.put(this.urlSuscriptores+perfil.idSuscriptor,perfil,this.httpHeaders).subscribe(data =>{
      console.log("PUT bien:");
        console.log(data['_body']);
      }, error =>{
        console.log("PUT mal:");
        console.log(error);
    });
  }

  consultarPerfil(id:string){
    return this.http.get(this.urlSuscriptores+id);
  }

  consultarSuscripciones(id:string){
    return this.http.get(this.urlSuscriptores+id+this.suscripciones)
  }

  consultarPublicacion(id:String){
    return this.http.get(this.urlPublicaciones+id);
  }

  consultarEjemplaresPorPublicacion(idS:String,idP:String){
    return this.http.get(this.urlSuscriptores+idS+this.ejemplares+this.barra+idP);
  }

  consultarEjemplar(id:String){
    return this.http.get(this.urlEjemplares+id);
  }
}

